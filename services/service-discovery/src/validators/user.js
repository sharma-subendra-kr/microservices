import joi from "joi";

export const signupAndLoginSchema = joi.object({
	email: joi.string().required(),
	password: joi.string().required(),
});
