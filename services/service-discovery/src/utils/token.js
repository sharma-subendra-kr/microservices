import jwt from "jsonwebtoken";

export const generateToken = (data) => {
	return jwt.sign(data, process.env.tokenSecret, {
		expiresIn: process.env.tokenExpire,
	});
};

export const checkToken = (token) => {
	return jwt.verify(token, process.env.tokenSecret);
};
