import express from "express";

import auth from "./auth";
import user from "./user";

const apiRouter = express.Router();

apiRouter.use("/auth", auth);
apiRouter.use("/user", user);

export default apiRouter;
