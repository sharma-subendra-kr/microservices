Running in dev DOCKER

1. To have spring boot restart on save only when reload-trigger.txt is changed:

* Uncomment these lines in application-dev.yml
  ```
	spring:
		devtools:
			restart:
				additional-paths: .
				trigger-file: reload-trigger.txt
	```
* use first one for manual trigger, second one for automatic trigger
  ```
	gradle build --continuous &
	OR
	gradle buildAndReload --continuous &
	```

2. To have spring boot restart on save when any files are changed:

* Comment these lines in application-dev.yml
  ```
	spring:
		devtools:
			restart:
				additional-paths: .
				trigger-file: reload-trigger.txt
	```
* use below command
  ```
	gradle build --continuous &
	```

Running in dev LOCAL

* Comment these lines in application-dev.yml
  ```
	spring:
		devtools:
			restart:
				additional-paths: .
				trigger-file: reload-trigger.txt
	```
* Run application using the run button
* Restart is triggered by build