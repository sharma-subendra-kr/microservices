package com.example.demo.controllers;

import com.example.demo.models.RequestCorrelation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping ("/api")
public class Hello {

	Logger logger = LoggerFactory.getLogger(Hello.class);

	@GetMapping ("/hello")
	public String hello () {
		logger.info("correlation-id {}", MDC.get(RequestCorrelation.CORRELATION_KEY));
		try {
			int a = 0;
			int b = 1;
			a = b / a;
		} catch (Exception e) {
			logger.info("Exception for correlation-id {} : {}", MDC.get(RequestCorrelation.CORRELATION_KEY), e);
		}
		return "Hello World!";
	}
}
