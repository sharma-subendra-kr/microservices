package com.example.demo.filters;

import com.example.demo.models.RequestCorrelation;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@Slf4j
@Component
@Order (1)
public class CorrelationHeaderFilter implements Filter {
	@Override
	public void doFilter (ServletRequest request, ServletResponse response, FilterChain chain)
		throws IOException, ServletException {
		final HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		final HttpServletResponse httpServletResponse = (HttpServletResponse) response;
		String correlationId = httpServletRequest.getHeader(RequestCorrelation.CORRELATION_KEY);
		log.info("correlation-id", correlationId);
		if (correlationId == null) {
			correlationId = UUID.randomUUID().toString();
			httpServletRequest.setAttribute(RequestCorrelation.CORRELATION_KEY, correlationId);
		}

		MDC.put("correlation-id", correlationId);
		httpServletResponse.setHeader(RequestCorrelation.CORRELATION_KEY, correlationId);
		chain.doFilter(request, response);
		MDC.clear();
	}
}
