import express from "express";
import bodyParser from "body-parser";
import { requestErrorHandler } from "../utils/requestErrorHandler";
import { signupController, loginController } from "../controllers/auth";
import { signupAndLoginSchema } from "../validators/user";
import CustomError from "../utils/CustomError";

const router = express.Router();
router.use(bodyParser.json());

router.post("/login", async (req, res) => {
	try {
		const { value, error } = signupAndLoginSchema.validate(req.body);
		if (error) {
			throw new CustomError("badRequest", "Invalid email or password");
		}
		const result = await loginController(value);

		res.json(result);
	} catch (error) {
		requestErrorHandler(res, error);
	}
});

router.post("/signup", async (req, res) => {
	try {
		const { value, error } = signupAndLoginSchema.validate(req.body);
		if (error) {
			throw new CustomError("badRequest", "Invalid email or password");
		}

		const result = await signupController(value);

		res.json(result);
	} catch (error) {
		console.log(error);
		requestErrorHandler(res, error);
	}
});

export default router;
