import { checkToken } from "../utils/token";

const exclude = ["auth"];

export const authMiddleware = (req, res, next) => {
	try {
		const path = req.path.split("/");

		if (exclude.indexOf(path[1]) !== -1) {
			next();
			return;
		}

		const details = checkToken(req.headers.authorization);
		req.user = details;
		next();
	} catch (error) {
		res.boom.unauthorized(error);
	}
};
