#include "../cpp_modules/crow_all.h"

using namespace std;

int main() {
	crow::SimpleApp app;

	CROW_ROUTE(app, "/")
	([]() { return "Hello World!"; });

	char *port = getenv("PORT");
	uint16_t iPort = static_cast<uint16_t>(port != NULL ? stoi(port) : 8080);
	cout << "PORT: " << iPort << "\n";
	app.port(iPort).multithreaded().run();
	return 0;
}
