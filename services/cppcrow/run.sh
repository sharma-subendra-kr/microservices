#!/bin/bash
source ./source.sh

port=$(lsof -t -i:$PORT -s TCP:LISTEN)
kill -9 $port

cd build
cmake ../src

make

./app

