# microservices

# Starting elk
	*	A minimum of 4GB RAM assigned to Docker
	*	Elasticsearch alone needs at least 2GB of RAM to run.
	*	On Linux, use sysctl vm.max_map_count on the host to view the current value, and see Elasticsearch's documentation on virtual memory for guidance on how to change this value. Note that the limits must be changed on the host; they cannot be changed from within a container.
	* check vm.max_map_count
		> sudo sysctl vm.max_map_count
	* change vm.max_map_count
		> sudo sysctl -w vm.max_map_count=262144
# Filebeats has to work in tls mode
	*	copy "logstash-beats.crt" to "services/demo"
	*	this has to be copied into docker container inside "/etc/pki/tls/certs/logstash-beats.crt", typically copied via shell script while starting main program inside docker container refer "demo" java spring boot app
